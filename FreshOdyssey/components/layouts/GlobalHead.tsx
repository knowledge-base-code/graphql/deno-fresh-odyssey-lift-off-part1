import { ComponentChildren } from 'preact'
import { asset, Head } from '$fresh/runtime.ts'

export interface IHeadContainerProps {
	children?: ComponentChildren
	title?: string
	name?: string
	description?: string
}

const CustomHead = ({ ...customMeta }) => {
	const meta = {
		title: 'learn fresh',
		description: 'Learn fresh using Next courses',
		type: 'website tutorial',
		...customMeta,
	}

	return (
		<Head>
			<title>{meta.title}</title>
			<meta content={meta.description} name='description' />
			<link rel='icon' href={asset('/favicon.ico')} />
			<link rel='stylesheet' href={asset('/index.css')} />
		</Head>
	)
}

export const HeadContainer = (
	{ children, ...customMeta }: IHeadContainerProps,
) => {
	return (
		<>
			<CustomHead {...customMeta} />
			<a href='https://fresh.deno.dev'>
				<img width='197' height='37' src='https://fresh.deno.dev/fresh-badge-dark.svg' alt='Made with Fresh' />
			</a>
			{children}
		</>
	)
}
